"""
A simple folder selector build on top of Ipywidgets.

Autor: H. Chauvet
"""
import os
import ipywidgets as widgets


def list_folders(path: str, search: [str, None] = None,
                 hidden: bool = False) -> list:
    """
    Get the list of folders from the given path.
    List of folder could be filtered by search term

    Parameters
    ----------

    path: string,
        The path to scann for folders

    search: string or None,
        The search term to filter the folder list

    Return a list of folder names
    """

    if path.startswith('~'):
        path = os.path.expanduser(path)

    abspath = os.path.abspath(os.path.dirname(path))
    listf = os.listdir(abspath)

    # Create a fullpath to those listed paths
    listf = (os.path.join(abspath, p) for p in listf)


    # Keep only basename and filter out files
    listf = (os.path.basename(p) for p in listf if os.path.isdir(p))

    if not hidden:
        listf = (p for p in listf if not p.strip().startswith('.'))

    if search is not None and len(search)>0:
        listf = (p for p in listf if search in p)

    return tuple(listf)


class SelectFolders():
    """
    A simple folder selector build on top of ipywidgets.
    """

    def __init__(self, init_path='./', autoclose=False, show_hidden=False,
                 load_callback=None, layout=widgets.Layout()):
        """
        autoclose: boolean,
            Do we need to autoclose the widget when select button is clicked. The default is True

        show_hidden: boolean,
            If True show hidden folders in folder list. Default is False.

        load_callback: function or None,
            Function called when the select button is clicked. Default action is None

        layout: None or ipywidgets.Layout object,
            Layout to pass to the VBox model
        """

        self.callback = load_callback
        self.selected_folders = []
        self.autoclose = autoclose
        self.hidden = show_hidden
        self.init_path = init_path

        self.wpath = widgets.Text(
            value=init_path,
            placeholder='Enter the path (../ to go back, / start at disk root, ~/ start at your Home)',
            description='',
            disabled=False,
            layout=widgets.Layout(width='auto')
        )

        self.wpathlist = widgets.SelectMultiple(
            value=[],
            options=list_folders(init_path, hidden=self.hidden),
            layout=widgets.Layout(min_height='110px', width='auto')
        )

        self.wselectbtn = widgets.Button(
            description='Select Folder(s)',
            disabled=True,
            button_style='info', # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Search or select a path in the list above',
            icon='caret-square-o-up',
            layout=widgets.Layout(width='auto')
        )

        # Connect to callback
        self.wpath.observe(self.autocomplete, names='value')
        self.wpathlist.observe(self.on_list_select, 'value')
        self.wselectbtn.on_click(self.on_click)

        # Build the layout
        self.box = widgets.AppLayout(header=self.wpath,
                                     left_sidebar=None,
                                     center=self.wpathlist,
                                     right_sidebar=None,
                                     footer=self.wselectbtn,
                                     layout=layout)

    def autocomplete(self, val):
        """
        Perform the autocompletion of the directory that match with the input
        path in val parameter
        """

        nval = val['new']

        # Do we add str to path or we are deleting the path
        forward = bool(len(val['new']) > len(val['old']))

        search_in_path = os.path.basename(nval).strip()

        if nval.strip() == '':
            nval = './'

        try:
            good_paths = list_folders(nval, search_in_path, hidden=self.hidden)
        except Exception as err:
            print(err)
            good_paths = None

        if good_paths is None:
            self.wpathlist.options = ['Not a valid Path']
            self.wselectbtn.disabled = True
            self.wselectbtn.button_style = 'danger'
            self.wselectbtn.description = 'Not a valid path!'
            self.wselectbtn.icon = 'times-circle-o'
            self.wpathlist.value = []
        else:
            # Update list of paths
            self.wpathlist.options = good_paths

            # Update the select button style
            if len(good_paths) == 0:
                # Check if it's a valid path (should end with "/"" if it's one)
                if self.wpath.value.endswith('/'):
                    self.wselectbtn.disabled = False
                    self.wselectbtn.button_style = 'success'
                    self.wselectbtn.description = 'Load %s folder' % os.path.basename(self.wpath.value[:-1]) # remove the last / in path str
                    self.wselectbtn.icon = 'check'
                else:
                    self.wselectbtn.disabled = True
                    self.wselectbtn.button_style = 'danger'
                    self.wselectbtn.description = 'Not a valid path!'
                    self.wselectbtn.icon = 'times-circle-o'

                self.wpathlist.value = []

            else:
                if len(self.wpathlist.value) == 0:
                    self.wselectbtn.disabled = True
                    self.wselectbtn.button_style = 'info'
                    self.wselectbtn.description = 'Select folder(s) [0/%i]' % (len(self.wpathlist.options))
                    self.wselectbtn.icon = 'caret-square-o-up'

            if forward:
                # Auto fill folder name if only one name remains
                if len(search_in_path)>0:
                    fstarts = tuple(p for p in good_paths if p.startswith(search_in_path))
                    if len(fstarts) == 1:
                        tmp = self.wpath.value
                        base = os.path.dirname(tmp)
                        lastdir = os.path.basename(tmp)
                        lastdir = lastdir.replace(lastdir, fstarts[0])
                        self.wpath.value = os.path.join(base, lastdir)

                # If only one good path found, enter to the folder if the name is completed
                if len(good_paths) == 1:
                    # Auto enter in the folder
                    if good_paths[0] == search_in_path:
                        self.wpath.value = self.wpath.value + '/'

    def on_list_select(self, val):
        """
        Callback when selected folder in list are changed
        """

        nselected = val['new']

        if len(nselected) > 0:
            self.wselectbtn.disabled = False
            self.wselectbtn.button_style = 'success'
            self.wselectbtn.description = 'Load (%i) folder' % len(nselected)
            self.wselectbtn.icon='check'
        else:
            self.wselectbtn.disabled = True
            self.wselectbtn.button_style = 'info'
            self.wselectbtn.description = 'Select folder(s) [0/%i]' % (len(self.wpathlist.options))
            self.wselectbtn.icon = 'caret-square-o-up'

    def on_click(self, val):
        """
        Call back when the load button is clicked
        """

        if '~' in self.wpath.value:
            rpath = os.path.expanduser(self.wpath.value)
        else:
            rpath = self.wpath.value

        if len(self.wpathlist.value) > 0:
            self.selected_folders = [os.path.abspath(os.path.join(os.path.dirname(rpath), p)) for p in self.wpathlist.value]
        else:
            self.selected_folders = [os.path.abspath(rpath)]

        if self.autoclose:
            self.box.close()

        if self.callback is not None:
            try:
                self.callback(self.selected_folders)
            except Exception as err:
                print(err)

    def __call__(self):
        return self.box
