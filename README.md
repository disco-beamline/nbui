# nbUI a collection of UI for jupyter notebook

This package provides a set of UI built on top of ipywidgets. UI are small sets of ipywidgets components that could be re-used in larger UI projects.

## Install

To install nbUI, download this repository, unpack it and run:

```bash
python3 -m pip install --user -U ./
```

Or directly from gitlab!

```bash
python3 -m pip install --user -U git+https://gitlab.com/discosoleil/nbui
```

## List of nbUI

### SelectFolders: A simple folders selection

Allow users to select one or multiple folders. Navigation in file system follow [Unix syntax](https://en.wikipedia.org/wiki/Path_(computing)) with autocompletion.
[demo](./demo_selectfolders.ipynb) 
[in nbviewver](https://nbviewer.jupyter.org/urls/gitlab.com/discosoleil/nbui/-/raw/master/demo_selectfolders.ipynb)

![selectfolders_screenshot](./docs/images/selectfolders.gif)


### SelectFiles: A simple file(s) selection

Allow users to select one or multiple files. 
[demo](./demo_selectfiles.ipynb)
[in nbviewver](https://nbviewer.jupyter.org/urls/gitlab.com/discosoleil/nbui/-/raw/master/demo_selectfiles.ipynb)